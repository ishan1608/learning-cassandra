package com.ishan1608.cassandramavenidea;

import com.datastax.driver.core.*;
import com.datastax.driver.core.policies.DCAwareRoundRobinPolicy;
import com.datastax.driver.core.policies.DefaultRetryPolicy;
import com.datastax.driver.core.policies.TokenAwarePolicy;
import com.datastax.driver.core.querybuilder.QueryBuilder;

import static com.datastax.driver.core.querybuilder.QueryBuilder.eq;

/**
 * Accessing cassandra with JAVA part 2
 */
public class GettingStartedTwo {

    public static void main(String[] args) {

        System.out.println("Running GettingStartedTwo.java");
        String keyspace = "demo_keyspace";
        String tableName = "users";

        // Connect to the cluster and keyspace "demo_keyspace"
        Cluster cluster = Cluster.builder()
                .addContactPoint("127.0.0.1")
                .withRetryPolicy(DefaultRetryPolicy.INSTANCE)
                .withLoadBalancingPolicy(new TokenAwarePolicy(
                        DCAwareRoundRobinPolicy.builder().build()))
                .build();
        Session session = cluster.connect(keyspace);

        // Create a table 'users'
        session.execute("CREATE TABLE IF NOT EXISTS users (firstname text, lastname text, age int, email text, city text, PRIMARY KEY (lastname))");

        /**
         * Insert one record into the users table using a PreparedStatement
         *
         * Using prepared a statement is  more secure and the most performant way
         * to get data into or out of our database. Prepared statements only need to
         * be parsed once by the cluster, and then then values are bound to variables
         * and then we execute the bound statement to read/write data from the cluster.
         */
		PreparedStatement statement = session.prepare(

		"INSERT INTO users" + "(lastname, age, city, email, firstname)"
				+ "VALUES (?,?,?,?,?);");

		BoundStatement boundStatement = new BoundStatement(statement);

		session.execute(boundStatement.bind("Jones", 37, "Austin",
				"bob@example.com", "Bob"));

        // Use select to get the user we just entered
		Statement select = QueryBuilder.select().all().from(keyspace, tableName)
				.where(eq("lastname", "Jones"));
        ResultSet results = session.execute(select);
		for (Row row : results) {
			System.out.format("%s %d \n", row.getString("firstname"),
					row.getInt("age"));
		}

        // Update the same user with a new age
		Statement update = QueryBuilder.update(keyspace, tableName)
				.with(QueryBuilder.set("age", 38))
				.where((eq("lastname", "Jones")));
                        session.execute(update);

        // Select and show the change
		select = QueryBuilder.select().all().from(keyspace, tableName)
				.where(eq("lastname", "Jones"));
		results = session.execute(select);
		for (Row row : results) {
            System.out.format("%s %d \n", row.getString("firstname"), row.getInt("age"));
        }

        // Delete the user from the users table
        Statement delete = QueryBuilder.delete().from(tableName)
                .where(eq("lastname", "Jones"));
        session.execute(delete);
        // Show that the user is gone
        select = QueryBuilder.select().all().from(keyspace, tableName);
        results = session.execute(select);
        for (Row row : results) {
            System.out.format("%s %d %s %s %s\n", row.getString("lastname"), row.getInt("age"), row.getString("city"),
                    row.getString("email"), row.getString("firstname"));
        }

        // Clean up the connection by closing it
        cluster.close();
    }
}
