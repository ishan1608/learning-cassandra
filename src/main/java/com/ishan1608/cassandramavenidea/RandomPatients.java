package com.ishan1608.cassandramavenidea;

import com.ishan1608.cassandramavenidea.patient.Provider;

/**
 * Sample to demonstrate the random names provider
 * Created by ishan on 17/5/16.
 */
public class RandomPatients {
    public static void main(String[] args) {
        Provider provider = new Provider();
        Provider.Patient patient = provider.getPatient();
        System.out.println(patient.getDescription());
    }
}
