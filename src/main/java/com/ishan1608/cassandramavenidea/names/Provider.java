package com.ishan1608.cassandramavenidea.names;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;


/**
 * Random names generator
 * Created by ishan on 17/5/16.
 */
public class Provider {
    private ArrayList<Cell> maleNameList;
    private ArrayList<Cell> femaleNameList;
    private ArrayList<Cell> lastNameList;

    private void initialize() {
        try {
            // Getting options.xlsx file from resources as InputStream
            InputStream namesFileInputStream = Provider.class.getClassLoader().getResourceAsStream("options.xlsx");
            // Finds the workbook instance for XLSX file
            XSSFWorkbook namesWorkbook = new XSSFWorkbook (namesFileInputStream);
            // Return male_name from the XLSX workbook
            XSSFSheet maleNameSheet = namesWorkbook.getSheet("male_name");
            // Caching the data in a List
            maleNameList = new ArrayList<Cell>();
            Iterator<Row> maleNameIterator = maleNameSheet.iterator();
            while (maleNameIterator.hasNext()) {
                Row row = maleNameIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                Cell cell = cellIterator.next();
                maleNameList.add(cell);
            }

            // Repeating the same for female names
            XSSFSheet femaleNameSheet = namesWorkbook.getSheet("female_name");
            // Caching the data in a List
            femaleNameList = new ArrayList<Cell>();
            Iterator<Row> femaleNameIterator = femaleNameSheet.iterator();
            while (femaleNameIterator.hasNext()) {
                Row row = femaleNameIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                Cell cell = cellIterator.next();
                femaleNameList.add(cell);
            }

            // Repeating the same for last names
            XSSFSheet lastNameSheet = namesWorkbook.getSheet("last_name");
            // Caching the data in a list
            lastNameList = new ArrayList<Cell>();
            Iterator<Row> lastNameIterator = lastNameSheet.iterator();
            while (lastNameIterator.hasNext()) {
                Row row = lastNameIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();
                Cell cell = cellIterator.next();
                lastNameList.add(cell);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public class Name {

        String firstName = null;
        String lastName = null;

        public String getFirstName() {
            return firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public String getName() {
            return String.format("%s %s", firstName, lastName);
        }
    }

    public Name getMaleName() {
        if (maleNameList == null || lastNameList == null) {
            initialize();
        }
        Name name = new Name();
        int pos = new Random().nextInt(maleNameList.size());
        name.firstName = maleNameList.get(pos).getStringCellValue();
        pos = new Random().nextInt(lastNameList.size());
        name.lastName = lastNameList.get(pos).getStringCellValue();
        return name;
    }

    public Name getFemaleName() {
        if (femaleNameList == null || lastNameList == null) {
            initialize();
        }
        Name name = new Name();
        int pos = new Random().nextInt(femaleNameList.size());
        name.firstName = femaleNameList.get(pos).getStringCellValue();
        pos = new Random().nextInt(lastNameList.size());
        name.lastName = lastNameList.get(pos).getStringCellValue();
        return name;
    }
}
